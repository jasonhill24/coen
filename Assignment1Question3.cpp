#include <iostream>
//#include "piglatin.h"
#include <string>
using namespace std;

/*
 * 1- if starts with aeiouyAEIOUY: append -way
 * 2- loop until first letter is a vowel, move first letter to the end
 * 3- no vowel: -way
 */
const string VOWELS = "aeiouyAEIOUY";
int main() {
    // use strings to find vowel
    string word;
    cin >> word;
    int index = word.find_first_of(VOWELS);
    if((index == 0) || (index == string::npos)) {
        word += "-way";
        cout << word;
    }
    else {
        word = word.substr(index) + "-" + word.substr(0, index) + "ay";
        cout << word;
    }
    return 0;
}
 
